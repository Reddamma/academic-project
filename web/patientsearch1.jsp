<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.app.utility.DBConnection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>INNOVA</title>
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Favicons
            ================================================== -->
        <link rel="shortcut icon" href="img44/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="img44/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img44/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img44/apple-touch-icon-114x114.png">

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css"  href="css44/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="fonts44/font-awesome/css/font-awesome.css">
                <style>
            .dropdown {
                position: relative;
                display: inline-block;
            }

            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                padding: 12px 16px;
                z-index: 1;
            }

            .dropdown:hover .dropdown-content {
                display: block;
            }
        </style>
        <style>
            .red {
                background-color: #666666;
            }
        </style>
        <!-- Stylesheet
            ================================================== -->
        <link rel="stylesheet" type="text/css" href="css44/style.css">
        <link rel="stylesheet" type="text/css" href="css44/nivo-lightbox/nivo-lightbox.css">
        <link rel="stylesheet" type="text/css" href="css44/nivo-lightbox/default.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
    </head>
    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
        <!-- Navigation
            ==========================================-->
               <nav id="menu" class="navbar navbar-default navbar-fixed-top">
            <div class="container"> 
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <a class="navbar-brand page-scroll" href="#page-top">Cloud</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="cloudhomepage.jsp" class="page-scroll">Home</a></li>
                 <li class="dropdown" class="page-scroll"><a href="#services" class="page-scroll">View Details</a>
                        
						<ul class="dropdown-content">
                                                 <li><a href="viewallendusers.jsp"><h5>&Diamond;&nbsp;View All End Users</h5></a>
                                                     <a href="patientsearch.jsp"><h5>&Diamond;&nbsp;View Specified Patient Record </h5></a>
                                                 </li>
						</ul>
					</li>
                                        
                       <li class="dropdown" class="page-scroll"><a href="#services" class="page-scroll">Patients Information</a>
                                                <ul class="dropdown-content">
                                                 <li><a href="viewallpatients.jsp"><h5>&Diamond;&nbsp;All Patients Records</h5></a>
                                                     <a href="patientdetails.jsp"><h5>&Diamond;&nbsp;Patient Details and EMR Records</h5></a>
                                                 </li>
						</ul></li>
                        <li class="dropdown" class="page-scroll"><a href="#testimonials" class="page-scroll">End User Information</a>
                       <ul class="dropdown-content">
                           <li><a href="transactions.jsp"><h5>&Diamond;&nbsp;End User Transactions</h5></a>
                            <a href="history.jsp"><h5>&Diamond;&nbsp;End User History</h5></a>
                        </li>
                       </ul>
                        </li>
                        <li class="dropdown" class="page-scroll"><a href="#testimonials" class="page-scroll">Health Measurements</a>
                           <ul class="dropdown-content">
                           <li>
                            <a href="minchart.jsp"><h5>&Diamond;&nbsp;View Minimum Health Measurements</h5></a>
                            <a href="viewmaximum.jsp"><h5>&Diamond;&nbsp;View Maximum Health Measurements</h5></a>
                        </li>
                       </ul>   
                        </li>
                        <li>
                             <a href="attackers.jsp" class="page-scroll">&nbsp;Attackers</a>
                        </li>
                        <li><a href="cloudlogout.jsp" class="page-scroll">Logout</a>
                    </ul>
                </div>
                <!-- /.navbar-collapse --> 
            </div>
        </nav>
        <br><br><br><br><br><br><br><br><br>
        <!-- Get Touch Section -->
        <div id="get-touch">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-1">
                        <h3>Specified Patient Records By Scalable Data Sharing</h3>

                        <br><br>
                    </div>
                </div><br>
            </div>
        </div>
        <%

            String name = request.getParameter("name");
            Connection con = DBConnection.getConnection();
            int bp = 0;
            int blood = 0;
            int cholestral = 0;
            int heartbeat = 0;
            int hyponatremia = 0;
            int potassium = 0;
            int sugar = 0;
            int sodium = 0;
            String sql1 = "select * from patientdetails  where name='" + name + "'";
            PreparedStatement ps1 = con.prepareStatement(sql1);
            ResultSet rs1 = ps1.executeQuery();
                    String colorcode = null;
            String colorcode1 = null;
            String colorcode2 = null;
            String colorcode3 = null;
            String colorcode4 = null;
            String colorcode5 = null;
            String colorcode6 = null;
            String colorcode7 = null;
            String status=null;
            String status1=null;
            String status2=null;
            String status3=null;
            String status4=null;
            String status5=null;
            String status6=null;
            String status7=null;
            String fstatus=null;
            String fstatus1=null;
            String fstatus2=null;
            String fstatus3=null;
            String fstatus4=null;
            String fstatus5=null;
            String fstatus6=null;
            String fstatus7=null;
            if (rs1.next()) {
                bp = rs1.getInt("bp");
                blood = rs1.getInt("blood");
                cholestral = rs1.getInt("cholestral");
                heartbeat = rs1.getInt("heartbeat");
                hyponatremia = rs1.getInt("hyponatremia");
                potassium = rs1.getInt("potassium");
                sugar = rs1.getInt("sugar");
                sodium = rs1.getInt("sodium");
                name = rs1.getString("name");
                if (bp >= 120 && bp <= 180) { 
                    colorcode = "green";
                    status="safe";
            fstatus="Good&nbsp;&nbsp;&#10004;";
                } else {
                    colorcode = "red";
                    status="hazardious";
                    fstatus="Not Good&nbsp;&nbsp;&#10060;";
                }
                
                if (blood >= 100 && blood <= 200) {
                    colorcode1 = "green";
                    status1="safe";
                    fstatus1="Good&nbsp;&nbsp;&#10004;";
                } else {
                    colorcode1 = "red";
                     status1="hazardious";
                     fstatus1="Not Good&nbsp;&nbsp;&#10060;";
                }
                if (cholestral >= 110 && cholestral <= 200) {
                    colorcode2 = "green";
                    status2="safe";
                    fstatus2="Good&nbsp;&nbsp;&#10004;";
                } else {
                    colorcode2 = "red";
                    status2="hazardious";
                    fstatus2="Not Good&nbsp;&nbsp;&#10060;";
                }
                if (heartbeat >= 2 && heartbeat <= 4) {
                    colorcode3 = "green";
                    status3="safe";
                    fstatus3="Good&nbsp;&nbsp;&#10004;";
                } else {
                    colorcode3 = "red";
                    status3="hazardious";
                    fstatus3="Not Good&nbsp;&nbsp;&#10060;";
                }
                if (hyponatremia >= 130 && hyponatremia <= 190) {
                    colorcode4 = "green";
                    status4="safe";
                    fstatus4="Good&nbsp;&nbsp;&#10004;";
                } else {
                    colorcode4 = "red";
                    status4="hazardious";
                    fstatus4="Not Good&nbsp;&nbsp;&#10060;";
                }
                if (potassium >= 100 && potassium <= 200) {
                    colorcode5 = "green";
                    status5="safe";
                    fstatus5="Good&nbsp;&nbsp;&#10004;";
                } else {
                    colorcode5 = "red";
                    status5="hazardious";
                    fstatus5="Not Good&nbsp;&nbsp;&#10060;";
                }
                if (sugar >= 140 && sugar <=200) {
                    colorcode6 = "green";
                    status6="safe";
                    fstatus6="Good&nbsp;&nbsp;&#10004;";
                } else {
                    colorcode6 = "red";
                    status6="hazardious";
                    fstatus6="Not Good&nbsp;&nbsp;&#10060;";
                }
                if (sodium >= 100 && sodium <= 200) {
                    colorcode7 = "green";
                    status7="safe";
                    fstatus7="Good&nbsp;&nbsp;&#10004;";
                } else {
                    colorcode7 = "red";
                    status7="hazardious";
                    fstatus7="Not Good&nbsp;&nbsp;&#10060;";
                }

        %>
        <h2>Patient Name:<%=name%></h2>
        <center><table border="3" height="400" bordercolor="black" width="800">
            <tr>
                <th colspan="3" class="red" style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Health Parameters</th>
                <th colspan="3" class="red" style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Minimum</th>
                <th colspan="3" class="red" style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Maximum</th>
                <th colspan="3" class="red" style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Current</th>
                <th colspan="3" class="red" style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Level</th>
                <th colspan="3" class="red" style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status</th>
            </tr>
            <tr><th colspan="3" class="red" style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BP Level</th><td colspan="3" align="center" >120</td><td colspan="3" align="center">180</td><td colspan="3" align="center" ><font color="<%=colorcode%>"><b><%=bp%></b></font></td><td colspan="3" align="center"><font color="<%=colorcode%>"><b><%=status%></b></font></td><td colspan="3" align="center"><font color="<%=colorcode%>"><b><%=fstatus%></b></td></tr>
            <tr><th colspan="3" class="red" style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Blood Level</th><td colspan="3" align="center">100</td><td colspan="3"align="center">200</td><td colspan="3" align="center"><font color="<%=colorcode1%>"><b><%=blood%></b></font></td><td colspan="3" align="center"><font color="<%=colorcode1%>"><b><%=status1%></b></font></td><td colspan="3" align="center"><font color="<%=colorcode1%>"><b><%=fstatus1%></b> </td></tr>
            <tr><th colspan="3" class="red" style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cholestral Level</th><td colspan="3" align="center">110</td> <td colspan="3" align="center">200 </td><td colspan="3" align="center"><font color="<%=colorcode2%>"><b><%=cholestral%></b></font></td><td colspan="3" align="center"><font color="<%=colorcode2%>"><b><%=status2%></b></font></td><td colspan="3"align="center"><font color="<%=colorcode2%>"><b><%=fstatus2%></b></td></tr>
            <tr><th colspan="3" class="red" style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HeartBeatLevel</th><td colspan="3" align="center">2</td><td colspan="3" align="center">4</td><td colspan="3" align="center"><font color="<%=colorcode3%>"><b><%=heartbeat%></b></font></td><td colspan="3" align="center"><font color="<%=colorcode3%>"><b><%=status3%></b></font></td><td colspan="3" align="center"><font color="<%=colorcode3%>"><b><%=fstatus3%></b></td></tr>
            <tr><th colspan="3" class="red" style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hyponatremia Level</th><td colspan="3" align="center">100</td><td colspan="3"align="center">190</td><td colspan="3" align="center"><font color="<%=colorcode4%>"><b><%=hyponatremia%></b></font></td><td colspan="3" align="center"><font color="<%=colorcode4%>"><b><%=status4%></b></font></td><td colspan="3" align="center"><font color="<%=colorcode4%>"><b><%=fstatus4%></b></td></tr>
            <tr><th colspan="3" class="red" style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Potassium Level</th><td colspan="3"align="center" >100</td><td colspan="3" align="center">200</td><td colspan="3" align="center"><font color="<%=colorcode5%>"><b><%=potassium%></b></font></td><td colspan="3" align="center"><font color="<%=colorcode5%>"><b><%=status5%></b></font></td><td colspan="3"align="center"><font color="<%=colorcode5%>"><b><%=fstatus5%></b></td></tr>
            <tr><th colspan="3" class="red" style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sugar Level</th><td colspan="3" align="center">140</td><td colspan="3" align="center">200</td><td colspan="3" align="center"><font color="<%=colorcode6%>"><b><%=sugar%></b></font></td><td colspan="3" align="center"><font color="<%=colorcode6%>"><b><%=status6%></b></font></td><td colspan="3" align="center"><font color="<%=colorcode6%>"><b><%=fstatus6%></b></td></tr>
            <tr><th colspan="3" class="red" style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sodium Level</th><td colspan="3" align="center">100 </td><td colspan="3" align="center">200</td><td colspan="3" align="center"><font color="<%=colorcode7%>"><b><%=sodium%></b></font></td><td colspan="3" align="center"><font color="<%=colorcode7%>"><b><%=status7%></b></font></td><td colspan="3" align="center"><font color="<%=colorcode7%>"><b><%=fstatus7%></b></td></tr>

        </table>
    </center><%}%><br><br>
    <!-- Footer Section -->
    <div id="footer">
        <div class="container text-center">
        </div>
    </div>
    <script type="text/javascript" src="js44/jquery.1.11.1.js"></script> 
    <script type="text/javascript" src="js44/bootstrap.js"></script> 
    <script type="text/javascript" src="js44/SmoothScroll.js"></script> 
    <script type="text/javascript" src="js44/nivo-lightbox.js"></script> 
    <script type="text/javascript" src="js44/jqBootstrapValidation.js"></script> 
    <script type="text/javascript" src="js44/contact_me.js"></script> 
    <script type="text/javascript" src="js44/main.js"></script>
</body>
</html>