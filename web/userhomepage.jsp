<!DOCTYPE html>
<html lang="en">
    <head>
        <title>User</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="The Venue template project">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="styles55/bootstrap-4.1.2/bootstrap.min.css">
        <link href="plugins55/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="plugins55/OwlCarousel2-2.2.1/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="plugins55/OwlCarousel2-2.2.1/owl.theme.default.css">
        <link rel="stylesheet" type="text/css" href="plugins55/OwlCarousel2-2.2.1/animate.css">
        <link href="plugins55/colorbox/colorbox.css" rel="stylesheet" type="text/css">
        <link href="plugins55/jquery-datepicker/jquery-ui.css" rel="stylesheet" type="text/css">
        <link href="plugins55/jquery-timepicker/jquery.timepicker.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="styles55/main_styles.css">
        <link rel="stylesheet" type="text/css" href="styles55/responsive.css">
    </head>
    <body>

        <div class="super_container">

            <!-- Header -->

            <header class="header">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="header_content d-flex flex-row align-items-center justify-content-start">
                                <div class="logo">
                                    <a href="userhomepage.jsp">
                                        <div>User</div>
                                        <div>HomePage</div>
                                    </a>
                                </div>
                                <nav class="main_nav">
                                    <ul class="d-flex flex-row align-items-center justify-content-start">
                        <li><a href="userhomepage.jsp">Home</a></li>
                        <li><a href="requestpermission.jsp">Request Permission</a></li>
                        <li><a href="requestdetails.jsp">Request Details</a></li>
                        <li><a href="searchfile.jsp">Search File</a></li>
                        <li><a href="searchhistory.jsp">Search History</a></li>
                        <li><a href="transactionhistory.jsp">Transaction History</a></li>
                        <li><a href="userlogout.jsp">Log Out</a></li>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <!-- Hamburger -->

            <div class="hamburger_bar trans_400 d-flex flex-row align-items-center justify-content-start">
                <div class="hamburger">
                    <div class="menu_toggle d-flex flex-row align-items-center justify-content-start">
                        <span>menu</span>
                        <div class="hamburger_container">
                            <div class="menu_hamburger">
                                <div class="line_1 hamburger_lines" style="transform: matrix(1, 0, 0, 1, 0, 0);"></div>
                                <div class="line_2 hamburger_lines" style="visibility: inherit; opacity: 1;"></div>
                                <div class="line_3 hamburger_lines" style="transform: matrix(1, 0, 0, 1, 0, 0);"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            	<div class="menu trans_800">
		<div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
			<ul>
				<li><a href="userhomepage.jsp">Home</a></li>
                        <li><a href="requestdetails.jsp">Request Details</a></li>
                        <li><a href="searchfile.jsp">Search File</a></li>
                        <li><a href="searchhistory.jsp">Search History</a></li>
                        <li><a href="transactionhistory.jsp">Transaction History</a></li>
                        <li><a href="logout.jsp">Log Out</a></li>
			</ul>
		</div>
	</div>

            <!-- Menu -->



            <!-- Home -->
<%
    String msg=request.getParameter("msg"); 
    System.out.println(msg);
       if(msg!=null && msg.equalsIgnoreCase("ask")){
        out.println("<script type=\"text/javascript\">");
        out.println("alert('Permission requested Already,please wait for TA Response ');");
        out.println("</script>");
         out.println("<font color='red'><b>Please wait for TA Response..they have not given access for you </b></font>");
    }
        if(msg!=null && msg.equalsIgnoreCase("granted")){
        out.println("<script type=\"text/javascript\">");
        out.println("alert('Already Permissions Granted By the TA');");
        out.println("</script>");
         out.println("<font color='green'><b>Permissions Already granted for you....</b></font>");
    }
        if(msg!=null && msg.equalsIgnoreCase("success")){
        out.println("<script type=\"text/javascript\">");
        out.println("alert('Request Succesfully Sent to TA');");
        out.println("</script>");
         out.println("<font color='green'><b>Request sent to TA...please wait they will process the request shortly</b></font>");
    }
        if(msg!=null && msg.equalsIgnoreCase("failed")){
        out.println("<script type=\"text/javascript\">");
        out.println("alert('Request Not Sent');");
        out.println("</script>");
         out.println("<font color='red'><b>Failed to Request to TA...</b></font>");
    }

%>
            <div class="home">
                <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images55/users.jpg" data-speed="0.8"></div>
                <div class="home_container">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="home_content text-center">
                                    <div class="home_subtitle page_subtitle"><font color="yellow">User HomePage</font></div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="scroll_icon"></div>
            </div>



            <footer class="footer">
                <div class="container">
                    <div class="row">

                        <!-- Footer Logo -->
                        <div class="col-lg-3 footer_col">
                            <div class="footer_logo">
                                <div class="footer_logo_title">Privacy</div>
                                <div class="footer_logo_subtitle">Preserving</div>
                            </div>
                            <div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                <p style="line-height: 1.2;">Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved </p>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
                        </div>

                        <!-- Footer About -->
                        <div class="col-lg-6 footer_col">
                            <div class="footer_about">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse cursus faucibus finibus. Ut non justo eleifend, facilisis nibh ut, interdum odio.</p>
                            </div>
                        </div>

                        <!-- Footer Contact -->
                        <div class="col-lg-3 footer_col">
                            <div class="footer_contact">
                                <ul>
                                    <li class="d-flex flex-row align-items-start justify-content-start">
                                        <div><div class="footer_contact_title">Address:</div></div>
                                        <div class="footer_contact_text">481 Creekside Lane Avila CA 93424</div>
                                    </li>
                                    <li class="d-flex flex-row align-items-start justify-content-start">
                                        <div><div class="footer_contact_title">Address:</div></div>
                                        <div class="footer_contact_text">+53 345 7953 32453</div>
                                    </li>
                                    <li class="d-flex flex-row align-items-start justify-content-start">
                                        <div><div class="footer_contact_title">Address:</div></div>
                                        <div class="footer_contact_text">yourmail@gmail</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <script src="js55/jquery-3.2.1.min.js "></script>
        <script src="styles55/bootstrap-4.1.2/popper.js"></script>
        <script src="styles55/bootstrap-4.1.2/bootstrap.min.js"></script>
        <script src="plugins55/greensock/TweenMax.min.js"></script>
        <script src="plugins55/greensock/TimelineMax.min.js"></script>
        <script src="plugins55/scrollmagic/ScrollMagic.min.js"></script>
        <script src="plugins55/greensock/animation.gsap.min.js"></script>
        <script src="plugins55/greensock/ScrollToPlugin.min.js"></script>
        <script src="plugins55/OwlCarousel2-2.2.1/owl.carousel.js"></script>
        <script src="plugins55/easing/easing.js"></script>
        <script src="plugins55/parallax-js-master/parallax.min.js"></script>
        <script src="plugins55/colorbox/jquery.colorbox-min.js"></script>
        <script src="plugins55/jquery-datepicker/jquery-ui.js"></script>
        <script src="plugins55/jquery-timepicker/jquery.timepicker.js"></script>
        <script src="js55/custom.js"></script>
    </body>
</html>