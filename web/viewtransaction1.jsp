<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.app.utility.DBConnection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Projection by TEMPLATED</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
                	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images11/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor11/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts11/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts11/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor11/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor11/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor11/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor11/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor11/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css11/util.css">
	<link rel="stylesheet" type="text/css" href="css11/main.css">
	</head>
	<body>

		<!-- Header -->
			<header id="header">
				<div class="inner">
                                    <a href="serviceproviderhome.jsp" class="logo"><strong><font color="gold">Service Provider</font></strong> </a>
					<nav id="nav">
                                            <a href="serviceproviderhome.jsp" ><font color="yellow">Home</font></a>
						<a href="upload.jsp">Upload</font>
						<a href="update.jsp">Update</font>
                                                <a href="viewtransaction.jsp">View Transaction</font>
                                                <a href="viewhistory.jsp">View History</font>
                                                <a href="servicelogout.jsp">Logout</font>
                                        </nav>
					<a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
				</div>
			</header>

		<!-- Banner -->
		<!-- Banner -->
			<section id="banner">
				
			</section>
                           
                                       <center>                  
               <table width="800" bordercolor="black" height="90" border="3">
                    <tr>
                        <th style="color:#003eff;font-size: 20">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S.No</th>
                       <th style="color:#003eff;font-size: 20">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UserName</th>
                       <th style="color:#003eff;font-size: 20">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Searched Patient</th>
                       <th style="color:#003eff;font-size: 20">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Secret Key </th>
                       <th style="color:#003eff;font-size: 20">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Task </th>
                       <th style="color:#003eff;font-size: 20">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date </th
                    </tr>
                    <%
        String name1=null;
        String name2=null;
        String date1=null;
        String skey=null;
        int sno=0;
        String name=request.getParameter("name");
    String sql="select * from Transaction where name='"+name+"' ";
    Connection con=DBConnection.getConnection();
   PreparedStatement ps=con.prepareStatement(sql);
   ResultSet rs=ps.executeQuery();
     if(rs.isBeforeFirst()){
    while(rs.next()){
 sno++;
name2=rs.getString("patientname");
date1=rs.getString("date");
skey=rs.getString("secretkey");
    %>
                    <tr>
                        <td style="color:purple">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><%=sno%></b></td>
                        <td style="color:purple">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><%=name%></b></td>
                        <td style="color:purple">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><%=name2%></b></td>
                        <td style="color:purple">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><%=skey%></b></td>
                        <td style="color:purple">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Download</b></td>
                        <td style="color:purple">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><%=date1%></b></td>

                    </tr>
    <%}}else{
out.println("<font color='red'>***NO Records Found on the Searched Name***</font>");
}%>
                </table></center>


        <!-- Footer Section -->
        <div id="footer">
            <div class="container text-center">
                <p>&copy; 2018  </p>
            </div>
        </div>
        <script type="text/javascript" src="js44/jquery.1.11.1.js"></script> 
        <script type="text/javascript" src="js44/bootstrap.js"></script> 
        <script type="text/javascript" src="js44/SmoothScroll.js"></script> 
        <script type="text/javascript" src="js44/nivo-lightbox.js"></script> 
        <script type="text/javascript" src="js44/jqBootstrapValidation.js"></script> 
        <script type="text/javascript" src="js44/contact_me.js"></script> 
        <script type="text/javascript" src="js44/main.js"></script>
    </body>
</html>