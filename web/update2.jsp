<%@page import="java.io.InputStream"%>
<%@page import="java.sql.Blob"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.app.utility.DBConnection"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>service</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons
    ================================================== -->
<link rel="shortcut icon" href="img44/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="img44/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="img44/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="img44/apple-touch-icon-114x114.png">

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css"  href="css44/bootstrap.css">
<link rel="stylesheet" type="text/css" href="fonts44/font-awesome/css/font-awesome.css">

<!-- Stylesheet
    ================================================== -->
<link rel="stylesheet" type="text/css" href="css44/style.css">
<link rel="stylesheet" type="text/css" href="css44/nivo-lightbox/nivo-lightbox.css">
<link rel="stylesheet" type="text/css" href="css44/nivo-lightbox/default.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css11/util.css">
	<link rel="stylesheet" type="text/css" href="css11/main.css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Navigation
    ==========================================-->
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand page-scroll" href="#page-top">Service</a>
      
    </div>  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
                                                <li><a href="serviceproviderhome.jsp" class="page-scroll">Home</a></li>
						<li><a href="upload.jsp" class="page-scroll">Upload</a></li>
						<li><a href="update.jsp" class="page-scroll">Update</a></li>
                                                <li><a href="viewtransaction.jsp" class="page-scroll">View Transaction</a></li>
                                                <li><a href="viewhistory.jsp" class="page-scroll">View History</a></li>
                                                <li><a href="servicelogout.jsp" class="page-scroll">Logout</a></li>
      </ul>
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <!-- /.navbar-collapse --> 
  </div>
</nav>
<!-- Header -->

<!-- Contact Section -->
<center>
<div id="contact">
  <div class="container">
    <div class="col-md-8">
      <div class="row">
        
   <%
       String name=request.getParameter("name");
            String is=null;
            Blob    b=null;
            String  dob=null;
            String  gender=null;
            String  email=null;
            String  mobile=null;
            String  address=null;
            String  city =null;
            int  pin =0;
        String sql="select * from patientdetails where name='"+name+"'";
        Connection con=DBConnection.getConnection();
        Statement stm=con.createStatement();
        ResultSet rs=stm.executeQuery(sql);
        while(rs.next()){
      is=rs.getString("file");
      dob=rs.getString("dob");
      gender=rs.getString("gender");
      email=rs.getString("email");
      mobile=rs.getString("mobile");
      address=rs.getString("address");
      city=rs.getString("city");
      pin=rs.getInt("pin");
      
        }%>               <div class="limiter">
            <form id="update" action="upload2?name=<%=name%>" method="post" >
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form ">
                                    <center>
    <h2 style="color:black">Update Patient Personal Details</h2>
        </center>
					<span style="float: left" class="login100-form-title p-b-43">
                                             
                                           <font style="float: left"> Patient Name:<font style="color:blue"><%=name%></font>
                                           </font>		</span>
					
					
					<div class="wrap-input100 validate-input" >
                                            <label style="color:black">Date Of Birth</label><input class="input1001" value="<%=dob%>"  type="text" name="dob">
						<span class="focus-input100"></span>
                                        </div><br>
					
					<div class="wrap-input100 validate-input" >
                                            <label style="color:black">Gender</label><input class="input1001"  value="<%=gender%>"  type="text" name="gender" >
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label style="color:black">Email</label><input class="input1001" value="<%=email%>" type="text"  name="email">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label style="color:black">Mobile</label><input class="input1001" value="<%=mobile%>" type="text"  name="mobile">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label style="color:black">Address</label><input class="input1001" value="<%=address%>" type="text"  name="address">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label style="color:black">City</label><input class="input1001" value="<%=city%>" type="text"  name="city">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label style="color:black">Pin</label><input class="input1001" value="<%=pin%>" type="text"  name="pin">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label style="color:black">Patient File Data</label><textarea  class="input1001"   name="file"><%=is%></textarea>
						<span class="focus-input100"></span>
                                        </div><br>                                    

					<div class="flex-sb-m w-full p-t-3 p-b-32">
					</div>
			

					


				</div>


                        </div></div></form></div>           


          <div id="success"></div>
          <button type="submit" form="update" class="btn btn-custom btn-lg">Update</button>
        </form></center>
      </div>
    </div>

  </div>
</div></center>
<!-- Footer Section -->
<div id="footer">
  <div class="container text-center">
  </div>
</div>
<script type="text/javascript" src="js44/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="js44/bootstrap.js"></script> 
<script type="text/javascript" src="js44/SmoothScroll.js"></script> 
<script type="text/javascript" src="js44/nivo-lightbox.js"></script> 
<script type="text/javascript" src="js44/jqBootstrapValidation.js"></script> 
<script type="text/javascript" src="js44/contact_me.js"></script> 
<script type="text/javascript" src="js44/main.js"></script>
</body>
</html>