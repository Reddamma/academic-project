<%@page import="java.io.InputStream"%>
<%@page import="java.sql.Blob"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.app.utility.DBConnection"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>INNOVA</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons

<!-- Bootstrap -->

<!-- Stylesheet
    ================================================== -->
<link rel="stylesheet" type="text/css" href="css44/style.css">
<link rel="stylesheet" type="text/css" href="css44/nivo-lightbox/default.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css11/main.css">
                <link rel="stylesheet" type="text/css" href="styles55/bootstrap-4.1.2/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="styles55/main_styles.css">
        <link rel="stylesheet" type="text/css" href="styles55/responsive.css">

</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

            <header class="header">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="header_content d-flex flex-row align-items-center justify-content-start">
                                <div class="logo">
                                    <a href="#">
                                        <div>User</div>
                                        <div>HomePage</div>
                                    </a>
                                </div>
                                <nav class="main_nav">
                                    <ul class="d-flex flex-row align-items-center justify-content-start">
                        <li><a href="userhomepage.jsp">Home</a></li>
                        <li><a href="requestdetails.jsp">Request Details</a></li>
                        <li><a href="searchfile.jsp">Search File</a></li>
                        <li><a href="searchhistory.jsp">Search History</a></li>
                        <li><a href="transactionhistory.jsp">Transaction History</a></li>
                        <li><a href="logout.jsp">Log Out</a></li>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

<!-- Contact Section -->
<center>
<div id="contact">
  <div class="container">
    <div class="col-md-8">
      <div class="row">
        
   <%
       String name=request.getParameter("name");
            String is=null;
            Blob b=null;
            int bp=0;
            int blood=0;
            int cholestral=0;
            int heartbeat=0;
            int hyponatremia=0;
            int potassium=0;
            int sugar=0;
            int sodium=0;
        String sql="select * from patientdetails where name='"+name+"'";
        Connection con=DBConnection.getConnection();
        Statement stm=con.createStatement();
        ResultSet rs=stm.executeQuery(sql);
        while(rs.next()){
        bp=rs.getInt("bp");
        blood=rs.getInt("blood");
        cholestral=rs.getInt("cholestral");
        heartbeat=rs.getInt("heartbeat");
        hyponatremia=rs.getInt("hyponatremia");
        potassium=rs.getInt("potassium");
        sugar=rs.getInt("sugar");
        sodium=rs.getInt("sodium");
      
        }%>               <div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form ">
                                    <center>
    <h2 style="color:black">Sensitive Patient Information</h2>
        </center>
					<span style="float: left" class="login100-form-title p-b-43">
                                             
                                           <font style="float: left"> Patient Name:<font style="color:red"><%=name%></font>
                                           </font>		</span><br><br><br>
					
					
					<div class="wrap-input100 validate-input" >
                                            <label style="color:black">Bp Level</label><input class="input1001" readonly value="<%=bp%>"  type="text" name="bp">
						<span class="focus-input100"></span>
                                        </div><br>
					
					<div class="wrap-input100 validate-input" >
                                            <label style="color:black">Blood Level</label><input class="input1001" readonly value="<%=blood%>"  type="text" name="blood" >
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label style="color:black">Cholestral Level</label><input class="input1001" readonly value="<%=cholestral%>" type="text"  name="cholestral">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label style="color:black">HeartBeat Level</label><input class="input1001" readonly value="<%=heartbeat%>" type="text"  name="heartbeat">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label style="color:black">Hyponatremia Level</label><input class="input1001" readonly value="<%=hyponatremia%>" type="text"  name="hyponatremia">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label style="color:black">Potassium Level</label><input class="input1001" readonly value="<%=potassium%>" type="text"  name="potassium">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label style="color:black">Sugar Level</label><input class="input1001" readonly value="<%=sugar%>" type="text"  name="sugar">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label style="color:black">Sodium Level</label><input class="input1001" readonly value="<%=sodium%>"   type="text" name="sodium">
						<span class="focus-input100"></span>
                                        </div><br>
<br>                                    

					<div class="flex-sb-m w-full p-t-3 p-b-32">
					</div>
			

					


				</div>


                        </div></div></div>           


          <div id="success"></div>
</center>
      </div>
    </div>

  </div>
</div></center>
<!-- Footer Section -->
<div id="footer">
  <div class="container text-center">
  </div>
</div>
<script type="text/javascript" src="js44/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="js44/bootstrap.js"></script> 
<script type="text/javascript" src="js44/SmoothScroll.js"></script> 
<script type="text/javascript" src="js44/nivo-lightbox.js"></script> 
<script type="text/javascript" src="js44/jqBootstrapValidation.js"></script> 
<script type="text/javascript" src="js44/contact_me.js"></script> 
<script type="text/javascript" src="js44/main.js"></script>
</body>
</html>