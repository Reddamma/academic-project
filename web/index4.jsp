<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login V11</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images5/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor5/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts5/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts5/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor5/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor5/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor5/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css5/util.css">
	<link rel="stylesheet" type="text/css" href="css5/main.css">
<!--===============================================================================================-->
</head>
<body>
		<div class="header_w3l">
	<div class="container">
            
		<nav class="main-nav navbar navbar-default">
			<div class="navbar-header">
				<h2 style="color: blue"><a  href="index.jsp"> Privacy Preservation for Outsourced
Medical Data 
<span> with Flexible Access
Control</span></a></h2>
			</div>         
				<!-- top-nav -->

		</nav>
	</div>
</div>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-30">
				<form id="cloud" action="cloudlogincheck.jsp" method="post" class="login100-form ">
					<span class="login100-form-title p-b-55">
                                            <font style="color:blue">Cloud Server Login</font>
					</span>

					<div class="wrap-input100 validate-input m-b-16" >
						<input class="input100" type="text" name="email" required placeholder="Username">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-envelope"></span>
						</span>
					</div>

					<div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
						<input class="input100" type="password" name="pass" required placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-lock"></span>
						</span>
					</div>
					
					<div class="container-login100-form-btn p-t-25">
						<button form="cloud" class="login100-form-btn">
							Login
						</button>
					</div>

				</form>	
                            <br><br>
                            <center><a class="txt2" href="index.jsp">
                                                    <h3 style="color: blueviolet">&leftarrow; Go Back</h3>
                                </a></center>
			   
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="vendor5/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor5/bootstrap/js/popper.js"></script>
	<script src="vendor5/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor5/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="js5/main.js"></script>

</body>
</html>