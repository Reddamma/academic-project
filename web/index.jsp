<!DOCTYPE html>
<html>
<head>
<title>PrivacyPreserving</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Clinical Lab Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>

<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel='stylesheet' type='text/css' href='css/jquery.easy-gallery.css' />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>


<link href='//fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<script src="js/responsiveslides.min.js"></script>
</head>
<body>
<!-- header -->
<div class="header_w3l">
	<div class="container">
            
		<nav class="main-nav navbar navbar-default">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<h3><a  href="index.jsp">Privacy Preservation for Outsourced
Medical Data 
<span> with Flexible Access
Control</span></a></h3>
			</div>         
				<!-- top-nav -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="main-nav nav navbar-nav navbar-right">
					<li><a href="index.jsp" class="active">Home</a></li>
					<li><a href="index2.jsp">Service provider</a></li>
					<li><a href="index4.jsp">Cloud Server</a></li>
                                        <li ><a href="userlogin.jsp">Users Department</a>
			
					</li>
					<li><a href="index3.jsp">Trusted Authority</a></li>
				</ul> <li><a href="attackerpage.jsp">Attacker</a></li>
				<div class="clearfix"> </div>	
			</div>
		</nav>
	</div>
</div>
<!-- header -->
<!-- banner -->
<div class="banner_w3ls w3layouts">
	<script>
						// You can also use "$(window).load(function() {"
						$(function () {
						 // Slideshow 4
						$("#slider3").responsiveSlides({
							auto: true,
							pager: true,
							nav: true,
							speed: 500,
							namespace: "callbacks",
							before: function () {
						$('.events').append("<li>before event fired.</li>");
						},
						after: function () {
							$('.events').append("<li>after event fired.</li>");
							}
							});
						});
	</script>
		<div  id="top" class="callbacks_container">
			<ul class="rslides" id="slider3">
				<li>
					<div class="banner-info w3">
						<div class="banner-text w3l">
							<h3>Privacy Preservation,<span>ABOUT OUR PATIENTS</span></h3>
							<p> provides privacy for patients information</p>
						</div>
					</div>
				</li>
				<li>
					<div class="banner-info w3ls">
						<div class="banner-text agileits">
							<h3>Security,<span>ROUND THE CLOCK</span></h3>
							<p>provides security for patients information</p>
						</div>
					</div>
				</li>
				<li>
					<div class="banner-info agileinfo">
						<div class="banner-text wthree">
							<h3>Electronic medical record.<span>LIVE YOUR HEALTHY LIFE</span></h3>
							<p>provides electronic medical record for patients information</p>
						</div>
					</div>
				</li>
			</ul>
			<div class="clearfix"></div>
		</div>
</div>
<!-- //banner -->
<!-- banner-bottom -->
<div class="bottom_wthree">
	<div class="col-md-6 bottom-left w3-agileits">	
		<figure class="cube-1">
			<div class="btm-hov">
				<div class="btm-wid">
					<div class="thumbs">
						<span class="rotate">
							<a href="#" class="btn">Abstract</a>
						</span>
					</div>
					<div class="fill_fig">
						<span class="fill"></span>
						<span class="fill"></span>
						<span class="fill"></span>
						<span class="fill"></span>
					</div>
				</div>
			</div>
		</figure>
		<div class="clearfix"></div>
	</div>
	<div class="col-md-6 bottom-right agileits-w3layouts">
		<div class="btm-right-grid agile">
			<h2>Abstract</h2>
                        <p style="text-align: justify">Electronic medical records (EMRs) play an important role in healthcare networks. Since
these records always contain considerable sensitive information regarding patients, privacy preservation
for the EMR system is critical. Current schemes usually authorize a user to read one?s EMR if and only
if his/her role satisfies the defined access policy. However, these existing schemes allow an adversary to
link patients? identities to their doctors. Therefore, classifications of patients? diseases are leaked without
adversaries actually seeing patients? EMRs. To address this problem, we present two anonymous schemes.
They not only achieve data confidentiality but also realize anonymity for individuals. The first scheme
achieves moderate security, where adversaries choose attack targets before obtaining information from the
EMR system. The second scheme achieves full security, where adversaries adaptively choose attack targets
after interaction with the EMR system. We provide rigorous proof showing the security and anonymity of
our schemes. In addition, we propose an approach in which EMR owners can search for their EMRs in an
anonymous system. For a better user experience, we apply the ?online/offline? approach to speed up data
processing. Experimental results show that the time complexity for key generation and EMR encapsulation
can be reduced to milliseconds. </p>
		</div>	
	</div>
	<div class="clearfix"></div>
</div>
<!-- //banner-bottom -->
<!-- services -->
<div class="services_agile">
	<div class="container">
		<h3 class="title">Our Best Services</h3>
		<div class="services_right w3-agile">
			<div class="col-md-4 list-left text-center wow bounceInDown" data-wow-duration="1.5s" data-wow-delay="0.1s">
				<span><img src="images/icon1.png" alt=" "/></span>
				<h4>Privacy Preservation</h4>
				<div class="multi-gd-text"><a href="#"><img class="img-responsive" src="images/privacy.jpg" alt=" "/></a></div>
				<p>patients privacy will be preserved</p>
			</div>
			<div class="col-md-4 list-left text-center wow bounceInDown" data-wow-duration="1.5s" data-wow-delay="0.2s">
				<span><img src="images/icon2.png" alt=" "/></span>
				<h4>Security</h4>
				<div class="multi-gd-text"><a href="#"><img class="img-responsive" src="images/security.jpg" alt=" "/></a></div>
				<p>security will be provided for patients data</p>
			</div>
			<div class="col-md-4 list-left text-center wow bounceInDown" data-wow-duration="1.5s" data-wow-delay="0.3s">
				<span><img src="images/icon3.png" alt=" "/></span>
				<h4>Electronic  Records</h4>
				<div class="multi-gd-text"><a href="#"><img class="img-responsive" src="images/medical.jpg" alt=" "/></a></div>
				<p>patients information can be stored in electronic medical records</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- //services -->



<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
			};
		*/								
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
</body>
</html>