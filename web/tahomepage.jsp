<html lang="en">
    <head>
        <title>The TA</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="The Venue template project">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="styles55/bootstrap-4.1.2/bootstrap.min.css">
        <link href="plugins55/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="plugins55/OwlCarousel2-2.2.1/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="plugins55/OwlCarousel2-2.2.1/owl.theme.default.css">
        <link rel="stylesheet" type="text/css" href="plugins55/OwlCarousel2-2.2.1/animate.css">
        <link href="plugins55/colorbox/colorbox.css" rel="stylesheet" type="text/css">
        <link href="plugins55/jquery-datepicker/jquery-ui.css" rel="stylesheet" type="text/css">
        <link href="plugins55/jquery-timepicker/jquery.timepicker.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="styles55/main_styles.css">
        <link rel="stylesheet" type="text/css" href="styles55/responsive.css">
    </head>
    <body>

        <div class="super_container">

            <!-- Header -->

            <header class="header">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="header_content d-flex flex-row align-items-center justify-content-start">
                                <div class="logo">
                                    <a href="tahomepage.jsp">
                                        <div>TA</div>
                                        <div>HomePage</div>
                                    </a>
                                </div>
                                <nav class="main_nav">
                                    <ul class="d-flex flex-row align-items-center justify-content-start">
                        							<li><a href="tahomepage.jsp">home</a></li>
								<li><a href="giveprivilages.jsp">Give Privilages </a></li>
								<li><a href="viewblockedattackers.jsp">View All Blocked Attackers</a></li>
                                                                <li><a href="useraccesscontrol.jsp">End User Access Control</a></li>
                                                                <li><a href="talogout.jsp">Logout</a></li></ul>

                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <!-- Hamburger -->

            <div class="hamburger_bar trans_400 d-flex flex-row align-items-center justify-content-start">
                <div class="hamburger">
                    <div class="menu_toggle d-flex flex-row align-items-center justify-content-start">
                        <span>menu</span>
                        <div class="hamburger_container">
                            <div class="menu_hamburger">
                                <div class="line_1 hamburger_lines" style="transform: matrix(1, 0, 0, 1, 0, 0);"></div>
                                <div class="line_2 hamburger_lines" style="visibility: inherit; opacity: 1;"></div>
                                <div class="line_3 hamburger_lines" style="transform: matrix(1, 0, 0, 1, 0, 0);"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            	<div class="menu trans_800">
		<div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
			<ul class="d-flex flex-row align-items-center justify-content-start">
								<li><a href="tahomepage.jsp">home</a></li>
								<li><a href="giveprivilages.jsp">Give Privilages </a></li>
								<li><a href="viewblockedattackers.jsp">View All Blocked Attackers</a></li>
                                                                <li><a href="useraccesscontrol.jsp">End User Access Control</a></li>
                                                                <li><a href="talogout.jsp">Logout</a></li>

							</ul>
		</div>
	</div>

            <!-- Menu -->



            <!-- Home -->

            <div class="home">
                <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images55/ts.jpg" data-speed="0.8"></div>
                <div class="home_container">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="home_content text-center">
                                    <div class="home_subtitle page_subtitle"><font color="yellow">The TA Homepage</font></div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- The Menu -->



					<!-- Dish -->
							

	<!-- Signature Dishes -->

	
	<!-- Reservations -->

	
	
</div>

<script src="js55/jquery-3.2.1.min.js"></script>
<script src="styles55/bootstrap-4.1.2/popper.js"></script>
<script src="styles55/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="plugins55/greensock/TweenMax.min.js"></script>
<script src="plugins55/greensock/TimelineMax.min.js"></script>
<script src="plugins55/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins55/greensock/animation.gsap.min.js"></script>
<script src="plugins55/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins55/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins55/easing/easing.js"></script>
<script src="plugins55/parallax-js-master/parallax.min.js"></script>
<script src="plugins55/jquery-datepicker/jquery-ui.js"></script>
<script src="plugins55/jquery-timepicker/jquery.timepicker.js"></script>
<script src="js55/menu.js"></script>
</body>
</html>