<!DOCTYPE html>
<html>
<head>
<title>Clinical Lab a Medical Category Flat Bootstrap Responsive Website Template | Treatments :: w3layouts</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Clinical Lab Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel='stylesheet' type='text/css' href='css/jquery.easy-gallery.css' />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->

<link href='//fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<script src="js/responsiveslides.min.js"></script>
</head>
<body>
<!-- header -->
<div class="header_w3l">
	<div class="container">
            
		<nav class="main-nav navbar navbar-default">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<h3><a  href="index.jsp">Privacy Preservation for Outsourced
Medical Data 
<span> with Flexible Access
Control</span></a></h3>
			</div>         
				<!-- top-nav -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="main-nav nav navbar-nav navbar-right">
					<li><a href="index.jsp" class="active">Home</a></li>
					<li><a href="index2.jsp">Service provider</a></li>
					<li><a href="index4.jsp">Cloud Server</a></li>
                                        <li class="has-dropdown"><a href="userlogin.jsp">Users Department</a>
						<ul class="dropdown">
                                                    <li><a href="userlogin.jsp">&nbsp;&nbsp;User Login</a></li>
                                                        <li><a href="register.jsp">&nbsp;&nbsp;User Registration</a></li>
						</ul>
					</li>
					<li><a href="index3.jsp">Trusted Authority</a></li>
				</ul> <li><a href="attackerpage.jsp">Attacker</a></li>
				<div class="clearfix"> </div>	
			</div>
		</nav>
	</div>
</div>
<!-- header -->
<!-- banner -->
   <% String msg=request.getParameter("msg");
       if(msg!=null && msg.equalsIgnoreCase("failed")){
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Failed to Login Please Provide Valid Details');");
                    out.println("</script>");
                     out.println("<h3><font color='red'><b>Invalid Login Credentials,please provide the correct details to Log in</b></font></h3>");
    }
      if(msg!=null && msg.equalsIgnoreCase("waiting")){
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('You are under waiting Status Please wait Cloud Server wil Activate You');");
                    out.println("</script>");
                     out.println("<h3><font color='green'><b>Cloud Server will get to you back very soon...</b></font></h3>");
    }
   %>
<!-- //banner -->
<!-- gallery -->
	<div class="gallery all_pad wthree">
		<div class="container">
			<div class="contact_w3agile">
	<div class="container">
		<h2 class="title agileinfo">User Login</h2>
		<div class="strip"></div>
 <br> <br> 
		<form id="log" action="userlogincheck.jsp" method="post">
			<input type="text"  placeholder="UserName" name="name" required onfocus="this.value='';" onblur="if (this.value == '') {this.value ='UserName';}">
			<input type="password" placeholder="Password" name="password" required onfocus="this.value='';" onblur="if (this.value == '') {this.value ='Password';}">
                        <br> <br> <br> <br>
			<div class="con-form text-center">
				<input form="log" type="submit" value="Submit">
			</div>
                </form><br>
                <br>
        </div><center><h3><a href="register.jsp"><font style="color: gold">Register here...</font></a></h3></center>
</div>
					<div class="clearfix"> </div>
				</div>
			   <div class="clearfix"> </div>
			    <script type='text/javascript' src='js/jquery.easy-gallery.js' ></script>
				<script type='text/javascript'>
				  //init Gallery
				  $('.gallery').easyGallery();
				</script>
			</div>

<!-- //gallery -->
<!-- contact -->

<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
			};
		*/								
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
</body>
</html>