
<!DOCTYPE html>
<html>
<head>
<title>Registration</title>
<!-- Meta tag Keywords -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Flat Sign Up Form Responsive Widget Template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Meta tag Keywords -->
<!-- css files -->
<link href="css1/style.css" rel="stylesheet" type="text/css" media="all">
<link href="css1/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
<!-- //css files -->
<!-- online-fonts -->
<link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'><link href='//fonts.googleapis.com/css?family=Raleway+Dots' rel='stylesheet' type='text/css'>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>

<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel='stylesheet' type='text/css' href='css/jquery.easy-gallery.css' />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>


<link href='//fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
        
</script>
<script type="text/javascript">
    function checkExist(){
               
                var xmlhttp = new XMLHttpRequest();
                var username = document.forms["regF"]["username"].value;
                var url = "exists.jsp?username=" + username;
                xmlhttp.onreadystatechange = function(){
                    if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                        if(xmlhttp.responseText == "\n\n\n\n\nUser already exists"){
                            document.getElementById("isE").style.color = "red";                          
                          document.getElementById('submit').disabled = false;
                          document.forms["regF"]["username"].value = " ";
                        }
                        else{
                            document.getElementById("isE").style.color = "green";
                           var resulotstatus =  xmlhttp.responseText.trim();
                          // alert("Result is "+resulotstatus);
                        document.getElementById("isE").innerHTML =resulotstatus;
                        // document.forms["regF"]["username"].value = "";
                       //  document.getElementById('submit').disabled = true;
                       if(resulotstatus=='exists'){
                           alert("UserName already Taken Please choose a new one ");
                         document.forms["regF"]["username"].value = "";
                         document.getElementById('submit').disabled = true;
                       }else{
                           
                                                       document.getElementById("isE").style.color = "green";
                           var resulotstatus =  xmlhttp.responseText.trim();
                          // alert("Result is "+resulotstatus);
                        document.getElementById("isE").innerHTML =resulotstatus;
                        document.getElementById('submit').disabled = false;
                       }
                    }
                    }
                    
                };
                try{
                xmlhttp.open("GET",url,true);
                xmlhttp.send();
            }catch(e){alert("unable to connect to server");
            }
        }
    </script>
<!-- start-smoth-scrolling -->
<script src="js/responsiveslides.min.js"></script>
</head>
<body >
<!--header-->
<div class="header_w3l">
	<div class="container">
            
		<nav class="main-nav navbar navbar-default">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<h3><a  href="index.jsp">Privacy Preservation for Outsourced
Medical Data 
<span> with Flexible Access
Control</span></a></h3>
			</div>         
				<!-- top-nav -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="main-nav nav navbar-nav navbar-right">
					<li><a href="index.jsp" class="active">Home</a></li>
					<li><a href="index2.jsp">Service provider</a></li>
					<li><a href="index4.jsp">Cloud Server</a></li>
                                        <li class="has-dropdown"><a href="userlogin.jsp">Users Department</a>
						<ul class="dropdown">
							<li><a href="userlogin.jsp">&nbsp;&nbsp;User Login</a>
                                                        <li><a href="register.jsp">&nbsp;&nbsp;User Registration</a></li>
						</ul>
					</li>
					<li><a href="index3.jsp">Trusted Authority</a></li>
				</ul>
				<div class="clearfix"> </div>	
			</div>
		</nav>
	</div>
</div>
<%
    String msg=request.getParameter("msg");

if(msg!=null && msg.equals("success")){
  out.println("<script type=\"text/javascript\">"); 
  out.println("alert('User Succesfully registered..');");
  out.println("</script>");
}
if(msg!=null && msg.equals("failed")){
  out.println("<script type=\"text/javascript\">"); 
  out.println("alert('Failed to register..';");
  out.println("</script>");
}
%>
<div class="main-agileits">
	<h2 class="sub-head">User Sign Up</h2>
		<div class="sub-main">	
			<form action="userregister" onsubmit="return validateForm()" method="post" name="regF" enctype="multipart/form-data">
				<input placeholder="User Name" name="username" class="name" type="text" onblur="checkExist()"/><span id="isE"></span> 
                                <input placeholder="Password" name="password" class="name" type="password" minlength="4" title="password length should be minimum 4 characters" required="">
                                <input placeholder="Email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="mail" type="text" required="">
                                <input placeholder="Phone Number" name="mobile" class="number" pattern="[6789][0-9]{9}" title="Please input integers" type="text" required="">
				<input  placeholder="Address" name="address" class="pass" type="text" required="">
                                <input  placeholder="dob E.G.:07/07/1994" pattern="\d{1,2}/\d{1,2}/\d{4}" class="datepicker" name="dob" class="pass" type="text" required="">
                                <br><label>Gender:</label><select name="gender">
                                    <option value="male">Male</option>
                                    <option value="female">FeMale</option>
                                </select><br>
                                <input  placeholder="pin" name="pin" class="pass" type="text" pattern="[456789][0-9]{5}" title="Please input 6 integers" required="">
                                <br>Service Provider Role
                                </label>
                                <select name="service">
                                    <option value="doctor">Out Patient Doctor</option>
                                    <option value="nurse">Out Patient Nurse</option>
                                </select><br><br>
                                <label> Profile Picture
                                </label><input  name="file" class="pass" type="file" required="">
                                <input type="submit" id="submit" value="sign up">
			</form>
		</div>
		<div class="clear"></div>
</div>
<!--//main-->

<!--footer-->
<div class="footer-w3">
	
</div>
<!--//footer-->

</body>
</html>