<%-- 
    Document   : talogout
    Created on : Nov 5, 2018, 3:39:49 PM
    Author     : saipratap
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            session.invalidate();
            response.sendRedirect("index3.jsp");
        %>
    </body>
</html>
