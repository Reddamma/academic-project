<%@page import="java.sql.Statement"%>
<%@page import="com.app.utility.DBConnection"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.sql.Blob"%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%
    String name=request.getParameter("name");
    String skey=request.getParameter("skey");
    String id=(String)request.getParameter("id");
    int BUFFER_SIZE = 4096;
    Connection con = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    int count=0;
    try {
        String username=(String)session.getAttribute("name");
        String query = "select file from patientdetails where name='"+name+"'";
        con = DBConnection.getConnection();
        ps = con.prepareStatement(query);
        rs = ps.executeQuery();
        if (rs.next()) {
            Blob blob = rs.getBlob("file");
            InputStream inputStream = blob.getBinaryStream();
            int fileLength = inputStream.available();

            System.out.println("fileLength = " + fileLength);

            ServletContext context = getServletContext();

            // sets MIME type for the file download
            String mimeType = context.getMimeType("file");
            if (mimeType == null) {
                mimeType = "application/octet-stream";
            }

            // set content properties and header attributes for the response
            response.setContentType(mimeType);
            response.setContentLength(fileLength);
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", "file");
            response.setHeader(headerKey, headerValue);

            // writes the file to the client
            OutputStream outStream = response.getOutputStream();
            java.sql.Timestamp t=new java.sql.Timestamp(new java.util.Date().getTime());
            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;
            count=count+1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            String sql="Insert into Transaction values(?,?,?,?)";
            PreparedStatement ps1=con.prepareStatement(sql);
            ps1.setString(1, username);
            ps1.setString(2,name);
            ps1.setString(3,skey);
            ps1.setTimestamp(4, t);
            int k=ps1.executeUpdate();
            if(k==0){
          response.sendRedirect("searchfile.jsp?notupdated");
            }
           inputStream.close();
           outStream.close();

        }
    } catch (Exception e) {
        System.out.println("Download Error " + e.getMessage());
        out.println(e);
        e.printStackTrace();
    } finally {
        try {
            // is.close();
            rs.close();
            ps.close();
            con.close();
        } catch (Exception e) {
        }
    }

%>