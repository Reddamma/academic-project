<%@page import="com.app.utility.DBConnection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Projection by TEMPLATED</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
                	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images11/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor11/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts11/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts11/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor11/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor11/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor11/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor11/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor11/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css11/util.css">
	<link rel="stylesheet" type="text/css" href="css11/main.css">
	</head>
	<body>

		<!-- Header -->
			<header id="header">
				<div class="inner">
                                    <a href="serviceproviderhome.jsp" class="logo"><strong><font color="gold">Service Provider</font></strong> </a>
					<nav id="nav">
                                            <a href="serviceproviderhome.jsp" ><font color="yellow">Home</font></a>
						<a href="upload.jsp">Upload</font>
						<a href="update.jsp">Update</font>
                                                <a href="viewtransaction.jsp">View Transaction</font>
                                                <a href="viewhistory.jsp">View History</font>
                                                <a href="servicelogout.jsp">Logout</font>
                                        </nav>
					<a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
				</div>
			</header>

		<!-- Banner -->
		<!-- Banner -->
			<section id="banner">
				
			</section>
                                       <% 
                    String msg=request.getParameter("msg");  
                    if(msg!=null && msg.equalsIgnoreCase("notpermitted")){
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('You are Not Authorized To Perform Search ');");
                    out.println("</script>");
                     out.println("<h3><font color='red'><b>Search Access is Not given by TA</b></font></h3>");
    }
                    if(msg!=null && msg.equalsIgnoreCase("noname")){
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('There is No Such Name in the DataBase ');");
                    out.println("</script>");
                     out.println("<h3><font color='red'><b>Please Provide Correct Name to perform Search </b></font></h3>");
    }
                                    %>
                                    <center>                  
                                        <form action="viewhistory1.jsp" id="search" method="post">
                                       &nbsp;&nbsp;&nbsp;&nbsp; <select  class="res_input " name ="name"  > 
                                        <br><br> 
         <%   String name=null;
        String sql="select * from userdetails";
        Connection con=DBConnection.getConnection();
        Statement stm=con.createStatement();
        ResultSet rs=stm.executeQuery(sql);
        while(rs.next()){
        name=rs.getString("name");
        %><option value="<%=name%>"><%=name%></option>  <%}%>               
                                  </select>
                                  <br>    <br><br>            
            <input type="submit" value=" Search ">
            </form>
    </center> 

 
	</body>
</html>