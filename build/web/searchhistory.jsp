<%@page import="com.app.utility.DBConnection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>The Venue</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="The Venue template project">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="styles55/bootstrap-4.1.2/bootstrap.min.css">
        <link href="plugins55/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="plugins55/OwlCarousel2-2.2.1/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="plugins55/OwlCarousel2-2.2.1/owl.theme.default.css">
        <link rel="stylesheet" type="text/css" href="plugins55/OwlCarousel2-2.2.1/animate.css">
        <link href="plugins55/colorbox/colorbox.css" rel="stylesheet" type="text/css">
        <link href="plugins55/jquery-datepicker/jquery-ui.css" rel="stylesheet" type="text/css">
        <link href="plugins55/jquery-timepicker/jquery.timepicker.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="styles55/main_styles.css">
        <link rel="stylesheet" type="text/css" href="styles55/responsive.css">
    </head>
    <body>

        <div class="super_container">

            <!-- Header -->

            <header class="header">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="header_content d-flex flex-row align-items-center justify-content-start">
                                <div class="logo">
                                    <a href="#">
                                        <div>User</div>
                                        <div>HomePage</div>
                                    </a>
                                </div>
                                <nav class="main_nav">
                                    <ul class="d-flex flex-row align-items-center justify-content-start">
                        <li><a href="userhomepage.jsp">Home</a></li>
                        <li><a href="requestdetails.jsp">Request Details</a></li>
                        <li><a href="searchfile.jsp">Search File</a></li>
                        <li><a href="searchhistory.jsp">Search History</a></li>
                        <li><a href="transactionhistory.jsp">Transaction History</a></li>
                        <li><a href="logout.jsp">Log Out</a></li>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <!-- Hamburger -->

            <div class="hamburger_bar trans_400 d-flex flex-row align-items-center justify-content-start">
                <div class="hamburger">
                    <div class="menu_toggle d-flex flex-row align-items-center justify-content-start">
                        <span>menu</span>
                        <div class="hamburger_container">
                            <div class="menu_hamburger">
                                <div class="line_1 hamburger_lines" style="transform: matrix(1, 0, 0, 1, 0, 0);"></div>
                                <div class="line_2 hamburger_lines" style="visibility: inherit; opacity: 1;"></div>
                                <div class="line_3 hamburger_lines" style="transform: matrix(1, 0, 0, 1, 0, 0);"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Menu -->



            <!-- Home -->

            <div class="home">
                <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images55/home.jpg" data-speed="0.8"></div>
                <div class="home_container">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="home_content text-center">
                                    <center>                  
                <table width="700" bordercolor="white" height="200" border="3">
                    <tr>
                        <th style="color:gold;font-size: 20">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S.No</th>
                        <th style="color:gold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UserName</th>
                        <th style="color:gold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Searched Patient</th>
                        <th style="color:gold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Permission </th>
                        <th style="color:gold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date </th
                    </tr>
                    <%
        String name1=null;
        String date1=null;
        int sno=0;
        String username=(String)session.getAttribute("name");
    String sql="select * from history  where username='"+username+"'";
    Connection con=DBConnection.getConnection();
   PreparedStatement ps=con.prepareStatement(sql);
   ResultSet rs=ps.executeQuery();
   if(rs.isBeforeFirst()){
    while(rs.next()){
 sno++;
name1=rs.getString("searchedname");
date1=rs.getString("date");
    %>
                    <tr>
                        <td style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=sno%></td>
                        <td style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=username%></td>
                        <td style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=name1%></td>
                        <td style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yes</td>
                        <td style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=date1%></td>

                    </tr>
    <%}}
    else{
out.println("<h2><font color='red'>***NO Records Found ***</font></h2>");

}%>
                </table></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="scroll_icon"></div>
            </div>



            <footer class="footer">
                <div class="container">
                    <div class="row">

                        <!-- Footer Logo -->
                        <div class="col-lg-3 footer_col">
                            <div class="footer_logo">
                                <div class="footer_logo_title">The Venue</div>
                                <div class="footer_logo_subtitle">restaurant</div>
                            </div>
                            <div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                <p style="line-height: 1.2;">Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
                        </div>

                        <!-- Footer About -->
                        <div class="col-lg-6 footer_col">
                            <div class="footer_about">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse cursus faucibus finibus. Ut non justo eleifend, facilisis nibh ut, interdum odio.</p>
                            </div>
                        </div>

                        <!-- Footer Contact -->
                        <div class="col-lg-3 footer_col">
                            <div class="footer_contact">
                                <ul>
                                    <li class="d-flex flex-row align-items-start justify-content-start">
                                        <div><div class="footer_contact_title">Address:</div></div>
                                        <div class="footer_contact_text">481 Creekside Lane Avila CA 93424</div>
                                    </li>
                                    <li class="d-flex flex-row align-items-start justify-content-start">
                                        <div><div class="footer_contact_title">Address:</div></div>
                                        <div class="footer_contact_text">+53 345 7953 32453</div>
                                    </li>
                                    <li class="d-flex flex-row align-items-start justify-content-start">
                                        <div><div class="footer_contact_title">Address:</div></div>
                                        <div class="footer_contact_text">yourmail@gmail</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <script src="js55/jquery-3.2.1.min.js "></script>
        <script src="styles55/bootstrap-4.1.2/popper.js"></script>
        <script src="styles55/bootstrap-4.1.2/bootstrap.min.js"></script>
        <script src="plugins55/greensock/TweenMax.min.js"></script>
        <script src="plugins55/greensock/TimelineMax.min.js"></script>
        <script src="plugins55/scrollmagic/ScrollMagic.min.js"></script>
        <script src="plugins55/greensock/animation.gsap.min.js"></script>
        <script src="plugins55/greensock/ScrollToPlugin.min.js"></script>
        <script src="plugins55/OwlCarousel2-2.2.1/owl.carousel.js"></script>
        <script src="plugins55/easing/easing.js"></script>
        <script src="plugins55/parallax-js-master/parallax.min.js"></script>
        <script src="plugins55/colorbox/jquery.colorbox-min.js"></script>
        <script src="plugins55/jquery-datepicker/jquery-ui.js"></script>
        <script src="plugins55/jquery-timepicker/jquery.timepicker.js"></script>
        <script src="js55/custom.js"></script>
    </body>
</html>