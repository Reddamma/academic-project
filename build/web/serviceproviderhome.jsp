<!DOCTYPE HTML>
<html>
	<head>
		<title>ServiceProvider</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>

		<!-- Header -->
			<header id="header">
				<div class="inner">
                                    <a href="serviceproviderhome.jsp" class="logo"><strong><font color="gold">Service Provider</font></strong> </a>
					<nav id="nav">
                                            <a href="serviceproviderhome.jsp" ><font color="yellow">Home</font></a>
						<a href="upload.jsp">Upload</font>
						<a href="update.jsp">Update</font>
                                                <a href="viewtransaction.jsp">View Transaction</font>
                                                <a href="viewhistory.jsp">View History</font>
                                                <a href="servicelogout.jsp">Logout</font>
                                        </nav>
					<a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
				</div>
			</header>

		<!-- Banner -->
			<section id="banner">
				<div class="inner">
					<header>
						<h1>Service Provider HomePage</h1>
					</header>

					<div class="flex ">

						<div>
							<div class="image round">
								<a href="upload.jsp"><img src="imagesn/uupload.jpg" alt="Pic 02" /></a>
							</div>
                                                        <h3><a href="upload.jsp">Upload</a></h3>
							<p>Upload Patient Details</p>
						</div>

						<div>
							<div class="image round">
                                                            <a href="update.jsp"><img src="imagesn/update.jpg" alt="Pic 02" /></a>
							</div>
							<h3><a href="update.jsp">Update</a></h3>
							<p>Update Patient Details</p>
						</div>

						<div>
							<div class="image round">
                                                            <a href="viewtransaction.jsp"><img src="imagesn/vi.jpg" alt="Pic 02" /></a>
							</div>
							<h3><a href="viewtransaction.jsp">View Transaction</a></h3>
							<p>view end user transactions</p>
						</div>
                                            <br<br>
                                            <br><br<br>
                                            <center><div>
							<div class="image round">
                                                            <a href="viewhistory.jsp"><img src="imagesn/history.jpg" alt="Pic 02" /></a>
							</div></center>
                                                        <h3><a href="viewhistory.jsp">View History</a></h3>
							<p>view enduser search history</p>
                                                </div>

					</div>


				</div>
			</section>




		<!-- Footer -->
			

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>