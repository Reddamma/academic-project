<%@page import="java.sql.Statement"%>
<%@page import="com.app.utility.DBConnection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Cloud</title>
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Favicons
            ================================================== -->
        <link rel="shortcut icon" href="img44/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="img44/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img44/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img44/apple-touch-icon-114x114.png">

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css"  href="css44/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="fonts44/font-awesome/css/font-awesome.css">

        <!-- Stylesheet
            ================================================== -->
        <link rel="stylesheet" type="text/css" href="css44/style.css">
        <link rel="stylesheet" type="text/css" href="css44/nivo-lightbox/nivo-lightbox.css">
        <link rel="stylesheet" type="text/css" href="css44/nivo-lightbox/default.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
        <style>
.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 12px 16px;
    z-index: 1;
}

.dropdown:hover .dropdown-content {
    display: block;
}
</style>
    </head>
    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
        <!-- Navigation
            ==========================================-->
        <nav id="menu" class="navbar navbar-default navbar-fixed-top">
            <div class="container"> 
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <a class="navbar-brand page-scroll" href="#page-top">Cloud</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="cloudhomepage.jsp" class="page-scroll">Home</a></li>
                 <li class="dropdown" class="page-scroll"><a href="#services" class="page-scroll">View Details</a>
                        
						<ul class="dropdown-content">
                                                 <li><a href="viewallendusers.jsp"><h5>&Diamond;&nbsp;View All End Users</h5></a>
                                                     <a href="patientsearch.jsp"><h5>&Diamond;&nbsp;View Specified Patient Record </h5></a>
                                                 </li>
						</ul>
					</li>
                                        
                       <li class="dropdown" class="page-scroll"><a href="#services" class="page-scroll">Patients Information</a>
                                                <ul class="dropdown-content">
                                                 <li><a href="viewallpatients.jsp"><h5>&Diamond;&nbsp;All Patients Records</h5></a>
                                                     <a href="patientdetails.jsp"><h5>&Diamond;&nbsp;Patient Details and EMR Records</h5></a>
                                                 </li>
						</ul></li>
                        <li class="dropdown" class="page-scroll"><a href="#testimonials" class="page-scroll">End User Information</a>
                       <ul class="dropdown-content">
                           <li><a href="transactions.jsp"><h5>&Diamond;&nbsp;End User Transactions</h5></a>
                            <a href="history.jsp"><h5>&Diamond;&nbsp;End User History</h5></a>
                        </li>
                       </ul>
                        </li>
                        <li class="dropdown" class="page-scroll"><a href="#testimonials" class="page-scroll">Health Measurements</a>
                           <ul class="dropdown-content">
                           <li>
                            <a href="minchart.jsp"><h5>&Diamond;&nbsp;View Minimum Health Measurements</h5></a>
                            <a href="viewmaximum.jsp"><h5>&Diamond;&nbsp;View Maximum Health Measurements</h5></a>
                        </li>
                       </ul>   
                        </li>
                        <li>
                             <a href="attackers.jsp" class="page-scroll">&nbsp;Attackers</a>
                        </li>
                        <li><a href="cloudlogout.jsp" class="page-scroll">Logout</a>
                    </ul>
                </div>
                <!-- /.navbar-collapse --> 
            </div>
        </nav>
        <br><br><br><br><br><br><br>
       
        <!-- Get Touch Section -->
        <div id="get-touch">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-1">
                        <br><br><br><h3>Specified Patient Records By Scalable Data Sharing</h3>
                        
<br><br>
                    </div>
                </div><br>
            </div>
        </div>
        <h1></h1><h1></h1><h1></h1>              

        <form action="patientsearch1.jsp" method="post">
           <center> <p text-align:center><b>Select Patient Name:<b></p><br><select  style="width:320px" class="form-control"  name="name">
                                <%
            String name=null;
        String sql="select * from patientdetails";
        Connection con=DBConnection.getConnection();
        Statement stm=con.createStatement();
        ResultSet rs=stm.executeQuery(sql);
        while(rs.next()){
        name=rs.getString("name");
        %><option value="<%=name%>"><%=name%></option>  <%}%>               
                                  </select>
                                  <br>                
            <input type="submit" value=" Search ">
            </form>
    </center> 



        <!-- Footer Section -->
        <div id="footer">
            <div class="container text-center">
                <p>&copy; 2017 Privacy Preserving</p>
            </div>
        </div>
        <script type="text/javascript" src="js44/jquery.1.11.1.js"></script> 
        <script type="text/javascript" src="js44/bootstrap.js"></script> 
        <script type="text/javascript" src="js44/SmoothScroll.js"></script> 
        <script type="text/javascript" src="js44/nivo-lightbox.js"></script> 
        <script type="text/javascript" src="js44/jqBootstrapValidation.js"></script> 
        <script type="text/javascript" src="js44/contact_me.js"></script> 
        <script type="text/javascript" src="js44/main.js"></script>
    </body>
</html>