<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.app.utility.DBConnection"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>serviceprovider</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons
    ================================================== -->
<link rel="shortcut icon" href="img44/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="img44/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="img44/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="img44/apple-touch-icon-114x114.png">

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css"  href="css44/bootstrap.css">
<link rel="stylesheet" type="text/css" href="fonts44/font-awesome/css/font-awesome.css">

<!-- Stylesheet
    ================================================== -->
<link rel="stylesheet" type="text/css" href="css44/style.css">
<link rel="stylesheet" type="text/css" href="css44/nivo-lightbox/nivo-lightbox.css">
<link rel="stylesheet" type="text/css" href="css44/nivo-lightbox/default.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Navigation
    ==========================================-->
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand page-scroll" href="#page-top">Service</a>
     
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
                                                <li><a href="serviceproviderhome.jsp" class="page-scroll">Home</a></li>
						<li><a href="upload.jsp" class="page-scroll">Upload</a></li>
						<li><a href="update.jsp" class="page-scroll">Update</a></li>
                                                <li><a href="viewtransaction.jsp" class="page-scroll">View Transaction</a></li>
                                                <li><a href="viewhistory.jsp" class="page-scroll">View History</a></li>
                                                <li><a href="servicelogout.jsp" class="page-scroll">Logout</a></li>
      </ul>
    </div><br>
    <br><div><center>
    <h2 style="color:black">Update Patient Details</h2>
        </center> </div>
    <!-- /.navbar-collapse --> 
  </div>
</nav>
<!-- Header -->

<!-- Contact Section -->
<center>
<div id="contact">
  <div class="container">
    <div class="col-md-8">
      <div class="row">
        <div class="section-title">
          <h2>Update</h2><br><br><br><br><br><br>
          <p>Please fill out the form below to update and we will get back to you as soon as possible:</p>
        </div>
          <%
    String msg=request.getParameter("msg");

if(msg!=null && msg.equals("success")){
  out.println("<script type=\"text/javascript\">"); 
  out.println("alert('User Details Updated..');");
  out.println("</script>");
}
if(msg!=null && msg.equals("failed")){
  out.println("<script type=\"text/javascript\">"); 
  out.println("alert('Failed to update UserDetails..';");
  out.println("</script>");
}
%>
        <form id="update" action="update1.jsp" novalidate style="float: right">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                  <center> <label><b>Patient Name<b></label><select  class="form-control" name="patientname">
     <%
                   String sql="select * from patientdetails";
        Connection con=DBConnection.getConnection();
        Statement stm=con.createStatement();
        ResultSet rs=stm.executeQuery(sql);
        while(rs.next()){
         String name=rs.getString("name");
        %><option value="<%=name%>"><%=name%></option>  <%}%>               
                                  </select>
                                      </center>
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                  <label><b>Details</b> <select style="width:50" class="form-control" name="details">
                          <option value="personal">Personal Details&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                          <option value="health">Health Details</option>
                      </select> <p class="help-block text-danger"></p>
              </div>
            </div>
          </div>

          <div id="success"></div>
          <button type="submit" form="update" class="btn btn-custom btn-lg">Update</button>
        </form></center>
      </div>
    </div>

  </div>
</div></center>
<!-- Footer Section -->
<div id="footer">
  <div class="container text-center">
  </div>
</div>
<script type="text/javascript" src="js44/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="js44/bootstrap.js"></script> 
<script type="text/javascript" src="js44/SmoothScroll.js"></script> 
<script type="text/javascript" src="js44/nivo-lightbox.js"></script> 
<script type="text/javascript" src="js44/jqBootstrapValidation.js"></script> 
<script type="text/javascript" src="js44/contact_me.js"></script> 
<script type="text/javascript" src="js44/main.js"></script>
</body>
</html>