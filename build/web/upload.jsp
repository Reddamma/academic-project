<!DOCTYPE HTML>
<html>
	<head>
		<title>service</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
                	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images11/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor11/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts11/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts11/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor11/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor11/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor11/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor11/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor11/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css11/util.css">
	<link rel="stylesheet" type="text/css" href="css11/main.css">
	</head>
	<body>

		<!-- Header -->
			<header id="header">
				<div class="inner">
                                    <a href="serviceproviderhome.jsp" class="logo"><strong><font color="gold">Service Provider</font></strong> </a>
					<nav id="nav">
                                            <a href="serviceproviderhome.jsp" ><font color="yellow">Home</font></a>
						<a href="upload.jsp">Upload</font>
						<a href="update.jsp">Update</font>
                                                <a href="viewtransaction.jsp">View Transaction</font>
                                                <a href="viewhistory.jsp">View History</font>
                                                <a href="servicelogout.jsp">Logout</font>
                                        </nav>
					<a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
				</div>
			</header>

		<!-- Banner -->
		<!-- Banner -->
			<section id="banner">
				
			</section>
   <%  
       String msg=request.getParameter("msg");  

       if(msg!=null && msg.equals("failed")){
  out.println("<script type=\"text/javascript\">"); 
  out.println("alert('Failed to upload..';");
  out.println("</script>");
}
       if(msg!=null && msg.equals("success")){
  out.println("<script type=\"text/javascript\">"); 
  out.println("alert('Details Uploaded');");
  out.println("</script>");
}
   %>
                <div class="limiter"><form id="upload" action="upload" method="post" enctype="multipart/form-data">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form validate-form">
					<span class="login100-form-title p-b-43">
						Health Details
					</span>
					
					
					<div class="wrap-input100 validate-input" >
                                            <label>Bp Level</label><input class="input100" required type="text" pattern="[0-9]+" title="please enter the value in integers" name="bp">
						<span class="focus-input100"></span>
                                        </div><br>
					
					<div class="wrap-input100 validate-input" >
                                            <label>Blood Level</label><input class="input100" pattern="[0-9]+" title="please enter the value in integers" required type="text" name="blood" >
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label>Cholestral Level</label><input class="input100" required type="text" pattern="[0-9]+" title="please enter the value in integers" name="cholestral">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label>HeartBeat Level</label><input class="input100" required type="text" pattern="[0-9]+" title="please enter the value in integers" name="heartbeat">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label>Hyponatremia Level</label><input class="input100" required type="text" pattern="[0-9]+" title="please enter the value in integers" name="hyponatremia">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label>Potassium Level</label><input class="input100" required type="text" pattern="[0-9]+" title="please enter the value in integers" required name="potassium">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label>Sugar Level</label><input class="input100" required type="text" pattern="[0-9]+" title="please enter the value in integers" name="sugar">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label>Sodium Level</label><input class="input100" required pattern="[0-9]+" title="please enter the value in integers"  type="text" name="sodium">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label>Patient File</label><input class="input100" required  type="file" name="file">
						<span class="focus-input100"></span>
                                        </div><br>                                    

					<div class="flex-sb-m w-full p-t-3 p-b-32">
					</div>
			

					


				</div>

				<div class="login100-more">
                                    <div class="login100-form validate-form">
					<span class="login100-form-title p-b-43">
						Personal Details
					</span>
					
					
					<div class="wrap-input100 " >
                                            <label>Name</label><input class="input100" type="text" required name="name">
						<span class="focus-input100"></span>
					
                                        </div><br>
					<div class="wrap-input100 validate-input" >
						<label>Date of Birth</label><input class="input100"  pattern="\d{1,2}/\d{1,2}/\d{4}" class="datepicker" required placeholder="ex.:dd/mm/yyyy" type="text" name="dob">
						<span class="focus-input100"></span>
                                                
					</div>	<br>				
					<div class="wrap-input100 validate-input" >
                                            <label>Gender</label><select name="gender">
                                                <option value="male">Male</option>
                                                <option value="female">FeMale</option>
                                            </select>
						<span class="focus-input100"></span>
                                        </div> <br>
					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                                            <label>Email</label><input class="input100" type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="mail" required name="email">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label>Mobile Number</label><input class="input100" required type="text" pattern="[6789][0-9]{9}" title="please enter 10 numbers" name="mobile">
						<span class="focus-input100"></span>
                                        </div> <br>
					<div class="wrap-input100 validate-input" >
                                            <label>Address</label><input class="input100" required type="text" name="address">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label>City</label><input class="input100" type="text" required name="city">
						<span class="focus-input100"></span>
                                        </div><br>
					<div class="wrap-input100 validate-input" >
                                            <label>Pin Code</label><input class="input100" type="text" pattern="[456789][0-9]{5}" title="Please input 6 integers" required name="pin">
						<span class="focus-input100"></span>
					</div>
                                      
					<div class="flex-sb-m w-full p-t-3 p-b-32">


					</div>
			

					
				<div>

				</div>
			</div>
                                      <div class="container-login100-form-btn">
						<button form="upload" class="login100-form-btn">
							Submit
						</button>
                                        </div>
		</div>
                        </div></div></form></div>
		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>
                        <script src="vendor11/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor11/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor11/bootstrap/js/popper.js"></script>
	<script src="vendor11/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor11/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor11/daterangepicker/moment.min.js"></script>
	<script src="vendor11/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor11/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js11/main.js"></script>

	</body>
</html>