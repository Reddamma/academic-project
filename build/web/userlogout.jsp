<%-- 
    Document   : userlogout
    Created on : Nov 5, 2018, 2:22:48 PM
    Author     : saipratap
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            session.invalidate();
        response.sendRedirect("userlogin.jsp");
        %>
    </body>
</html>
