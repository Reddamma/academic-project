
package com.app.action;

import com.app.utility.DBConnection;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@MultipartConfig( maxFileSize = Long.MAX_VALUE)
 public class upload extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException { System.out.println("yes");
   String sql="Insert into patientdetails (bp,blood,cholestral,heartbeat,hyponatremia,potassium,sugar,sodium,name,dob,gender,email,mobile,address,city,pin,file,secretkey) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try  
        { 
            System.out.println("yes");
        int bp           =Integer.parseInt(request.getParameter("bp"));
        int  blood       =Integer.parseInt(request.getParameter("blood"));
        int  cholestral  =Integer.parseInt(request.getParameter("cholestral"));
        int  heartbeat   =Integer.parseInt(request.getParameter("heartbeat"));
        int  hyponatremia=Integer.parseInt(request.getParameter("hyponatremia"));
        int  potassium   =Integer.parseInt(request.getParameter("potassium"));
        int  sugar       =Integer.parseInt(request.getParameter("sugar"));
        int  sodium      =Integer.parseInt(request.getParameter("sodium"));
        //personaldetails
         System.out.println("yes");
        String  name    =request.getParameter("name");
        String  dob     =request.getParameter("dob");
        String  gender  =request.getParameter("gender");
        String  email   =request.getParameter("email");
        String  mobile  =request.getParameter("mobile");
        String  address =request.getParameter("address");
        String  city    =request.getParameter("city");
        int  pin     =Integer.parseInt(request.getParameter("pin"));
        RandomkeyGenerator rkg=new RandomkeyGenerator();
         String skey=rkg.generateSecretKey();
        //System.out.println(bp+blood+cholestral+heartbeat+hyponatremia+potassium+sugar+sodium+name+dob+gender+email+mobile+address+city+pin);
            Part filepart=request.getPart("file");
            InputStream is=null;
            InputStream is1=null;
            if(filepart!=null){
                is=filepart.getInputStream();
            }
            String ename=EncryptionAlgoritham.encrypt(name);
            String edob=EncryptionAlgoritham.encrypt(dob);
            String egender=EncryptionAlgoritham.encrypt(gender);
            String eemail=EncryptionAlgoritham.encrypt(email);
            String emobile=EncryptionAlgoritham.encrypt(mobile);
            String eaddress=EncryptionAlgoritham.encrypt(address);
            String ecity=EncryptionAlgoritham.encrypt(city);
            String epin=EncryptionAlgoritham.encrypt(request.getParameter("pin"));
            String ebp=EncryptionAlgoritham.encrypt(request.getParameter("bp"));
            String eblood=EncryptionAlgoritham.encrypt(request.getParameter("blood"));
            String echolestral=EncryptionAlgoritham.encrypt(request.getParameter("cholestral"));
            String eheartbeat=EncryptionAlgoritham.encrypt(request.getParameter("heartbeat"));
            String ehyponatremia=EncryptionAlgoritham.encrypt(request.getParameter("hyponatremia"));
            String epotassium=EncryptionAlgoritham.encrypt(request.getParameter("potassium"));
            String esugar=EncryptionAlgoritham.encrypt(request.getParameter("sugar"));
            String esodium=EncryptionAlgoritham.encrypt(request.getParameter("sodium"));
            String sql1="insert into encrypteddetails (bp,blood,cholestral,heartbeat,hyponatremia,potassium,sugar,sodium,name,dob,gender,email,mobile,address,city,pin) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
            Connection con=DBConnection.getConnection();
            PreparedStatement ps=con.prepareStatement(sql1);
            ps.setString(1, ebp);
            ps.setString(2, eblood);
            ps.setString(3, echolestral);
            ps.setString(4, eheartbeat);
            ps.setString(5, ehyponatremia);
            ps.setString(6, epotassium);
            ps.setString(7, esugar);
            ps.setString(8, esodium);
            ps.setString(9, ename);
            ps.setString(10, edob);
            ps.setString(11, egender);
            ps.setString(12, eemail);
            ps.setString(13, emobile);
            ps.setString(14, eaddress);
            ps.setString(15, ecity);
            ps.setString(16, epin);
            int k1=ps.executeUpdate();
            if(k1>0){
            PreparedStatement p;   
            p = con.prepareStatement(sql);
            p.setInt(1, bp);
            p.setInt(2, blood);
            p.setInt(3, cholestral);
            p.setInt(4, heartbeat);
            p.setInt(5, hyponatremia);
            p.setInt(6, potassium);
            p.setInt(7, sugar);
            p.setInt(8, sodium);
            p.setString(9, name);
            p.setString(10, dob);
            p.setString(11, gender);
            p.setString(12, email);
            p.setString(13, mobile);
            p.setString(14, address);
            p.setString(15, city);
            p.setInt(16, pin);
            p.setBinaryStream(17, filepart.getInputStream());
            p.setString(18,skey);
            int k=p.executeUpdate();
          if(k>0){
              System.out.println("updated");
              response.sendRedirect("upload.jsp?msg=success");
             
          }
          else{
response.sendRedirect("upload.jsp?msg=failed");
             
              
          }}          else{
response.sendRedirect("upload.jsp?msg=encfailed");
             
              
          }
        }
        catch(Exception e){
            out.println(e);
            e.printStackTrace();
            e.getMessage();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
