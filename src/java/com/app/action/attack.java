
package com.app.action;

import com.app.utility.DBConnection;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

public class attack extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name=request.getParameter("name");
        String hname=request.getParameter("hname");
       
       response.setContentType("text/html;charset=UTF-8");
       
        PrintWriter out = response.getWriter();
        try  
        { 

   String sql="Insert into attacker  (attackername,username,bp,blood,cholestral,heartbeat,hyponatremia,potassium,sugar,sodium,file,date,status) values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
   
        int  bp              =Integer.parseInt(request.getParameter("bp"));
        int  blood       =Integer.parseInt(request.getParameter("blood"));
        int  cholestral  =Integer.parseInt(request.getParameter("cholestral"));
        int  heartbeat   =Integer.parseInt(request.getParameter("heartbeat"));
        int  hyponatremia=Integer.parseInt(request.getParameter("hyponatremia"));
        int  potassium   =Integer.parseInt(request.getParameter("potassium"));
        int  sugar       =Integer.parseInt(request.getParameter("sugar"));
        int  sodium      =Integer.parseInt(request.getParameter("sodium"));
        String data=request.getParameter("file");
        InputStream is = new ByteArrayInputStream(data.getBytes(Charset.forName("UTF-8")));
            PreparedStatement p;   
            java.sql.Timestamp t=new java.sql.Timestamp(new java.util.Date().getTime());
            Connection con=DBConnection.getConnection();
            p = con.prepareStatement(sql);
            p.setString(1, hname);
            p.setString(2, name);
            p.setInt(3, bp);
            p.setInt(4, blood);
            p.setInt(5, cholestral);
            p.setInt(6, heartbeat);
            p.setInt(7, hyponatremia);
            p.setInt(8, potassium);
            p.setInt(9, sugar);
            p.setInt(10, sodium);
            p.setBinaryStream(11, is);
            p.setTimestamp(12,t);
            p.setString(13,"Active");
            int k=p.executeUpdate();
          if(k>0){

              System.out.println("updated");
              RequestDispatcher rd=request.getRequestDispatcher("attacked.jsp?msg=success");
              rd.forward(request, response);
          }
          else{
              response.sendRedirect("attackerpage.jsp?msg=failed");
          }}
        catch(Exception e){
            out.println(e);
            e.printStackTrace();
            e.getMessage();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
