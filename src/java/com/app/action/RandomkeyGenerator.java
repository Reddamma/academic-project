
package com.app.action;

import java.util.Random;

public class RandomkeyGenerator {
  private static final String CHAR_LIST="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";  
  private static final int RANDOM_STRING_LENGTH= 6;
  public String generateSecretKey(){
      StringBuffer sb=new StringBuffer();
      for(int i=0;i<RANDOM_STRING_LENGTH;i++){
          int number=getRandomNumber();
          char ch=CHAR_LIST.charAt(number);
          sb.append(ch);
      }
      return sb.toString();
  }
  private int getRandomNumber(){
      int randomnumber=0;
      Random randomgenerator=new Random();
      randomnumber=randomgenerator.nextInt(CHAR_LIST.length());
      if(randomnumber -1 ==-1){
          return randomnumber;
      }
      else{
          return randomnumber -1;
      }
  }
}