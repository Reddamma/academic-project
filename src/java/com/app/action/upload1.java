
package com.app.action;

import com.app.utility.DBConnection;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
@MultipartConfig( maxFileSize = Long.MAX_VALUE)
public class upload1 extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name=request.getParameter("name");
   String sql="update patientdetails set bp=?,blood=?,cholestral=?,heartbeat=?,hyponatremia=?,potassium=?,sugar=?,sodium=?,file=? where name='"+name+"'";
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try  
        { 
            
        int  bp              =Integer.parseInt(request.getParameter("bp"));
        int  blood       =Integer.parseInt(request.getParameter("blood"));
        int  cholestral  =Integer.parseInt(request.getParameter("cholestral"));
        int  heartbeat   =Integer.parseInt(request.getParameter("heartbeat"));
        int  hyponatremia=Integer.parseInt(request.getParameter("hyponatremia"));
        int  potassium   =Integer.parseInt(request.getParameter("potassium"));
        int  sugar       =Integer.parseInt(request.getParameter("sugar"));
        int  sodium      =Integer.parseInt(request.getParameter("sodium"));
        String data=request.getParameter("file");
        InputStream is = new ByteArrayInputStream(data.getBytes(Charset.forName("UTF-8")));
            Connection con=DBConnection.getConnection();
            PreparedStatement p;   
            p = con.prepareStatement(sql);
            p.setInt(1, bp);
            p.setInt(2, blood);
            p.setInt(3, cholestral);
            p.setInt(4, heartbeat);
            p.setInt(5, hyponatremia);
            p.setInt(6, potassium);
            p.setInt(7, sugar);
            p.setInt(8, sodium);
            p.setBinaryStream(9, is);
            int k=p.executeUpdate();
          if(k>0){
              System.out.println("updated");
              RequestDispatcher rd=request.getRequestDispatcher("update.jsp?msg=success");
              rd.forward(request, response);
          }
          else{
              
          }
            
            
        }
        catch(Exception e){
            out.println(e);
            e.printStackTrace();
            e.getMessage();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
