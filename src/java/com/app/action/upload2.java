
package com.app.action;

import com.app.utility.DBConnection;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
@MultipartConfig( maxFileSize = Long.MAX_VALUE)
public class upload2 extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try  
        { 
            String data=request.getParameter("file");
            InputStream is=new ByteArrayInputStream(data.getBytes(Charset.forName("UTF-8")));
        String  name    =request.getParameter("name");
        String  dob     =request.getParameter("dob");
        String  gender  =request.getParameter("gender");
        String  email   =request.getParameter("email");
        String  mobile  =request.getParameter("mobile");
        String  address =request.getParameter("address");
        String  city    =request.getParameter("city");
        int  pin     =Integer.parseInt(request.getParameter("pin"));
           String sql="update patientdetails set name=?,dob=?,gender=?,email=?,mobile=?,address=?,city=?,pin=?,file=? where name='"+name+"'";

        //System.out.println(bp+blood+cholestral+heartbeat+hyponatremia+potassium+sugar+sodium+name+dob+gender+email+mobile+address+city+pin);

            Connection con=DBConnection.getConnection();
            PreparedStatement p;   
            p = con.prepareStatement(sql);
            p.setString(1, name);
            p.setString(2, dob);
            p.setString(3, gender);
            p.setString(4, email);
            p.setString(5, mobile);
            p.setString(6, address);
            p.setString(7, city);
            p.setInt(8, pin);
            p.setBinaryStream(9,is);
            int k=p.executeUpdate();
          if(k>0){
              System.out.println("updated");
              response.sendRedirect("update.jsp?msg=success");
    
          }
          else{
              response.sendRedirect("update.jsp?msg=failed");
          }
            
            
        }
        catch(Exception e){
            out.println(e);
            e.printStackTrace();
            e.getMessage();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
