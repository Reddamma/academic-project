
package com.app.action;

import com.app.utility.DBConnection;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@MultipartConfig(maxFileSize = Long.MAX_VALUE)
public class userregister extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try  {
               String sql="Insert into userdetails(name,password,dob,gender,email,mobile,address,service,file,status,search,download,permission)  values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
       String status="waiting";
        String  name    =request.getParameter("username");
        String  password    =request.getParameter("password");
        String  dob     =request.getParameter("dob");
        String  gender  =request.getParameter("gender");
        String  email   =request.getParameter("email");
        String  mobile  =request.getParameter("mobile");
        String  address =request.getParameter("address");
        String  service    =request.getParameter("service");
         Part filepart=request.getPart("file");
            InputStream is=null;
        int  pin     =Integer.parseInt(request.getParameter("pin"));
Connection con=DBConnection.getConnection();
            PreparedStatement p;   
            p = con.prepareStatement(sql);
            p.setString(1, name);
            p.setString(2, password);
            p.setString(3, dob);
            p.setString(4, gender);
            p.setString(5, email);
            p.setString(6, mobile);
            p.setString(7, address);
            p.setString(8, service);     
            p.setBinaryStream(9, filepart.getInputStream());
            p.setString(10, status);  
            p.setString(11, "notgiven");  
            p.setString(12, "notgiven");  
            p.setString(13, "no"); 
            int k=p.executeUpdate();
          if(k>0){
              System.out.println("updated");
              response.sendRedirect("register.jsp?msg=success");
              
          }
          else{
              System.out.println("not inserted");
              response.sendRedirect("register.jsp?msg=failed");
              
          }
            
            
        }
        catch(Exception e){
            out.println(e);
            e.printStackTrace();
            e.getMessage();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
